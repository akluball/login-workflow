import { expect } from 'chai';
import { WebDriver, until, WebElement, By, WebElementPromise } from 'selenium-webdriver';

const timeoutMillis = 3000;

export class WebDriverWaiter {
    constructor(private driver: WebDriver) { }

    async title(expectedTitle: string): Promise<void> {
        await this.driver.wait(until.titleIs(expectedTitle), timeoutMillis)
            .catch(async e => {
                const actualTitle = await this.driver.getTitle();
                expect(actualTitle).to.equal(expectedTitle);
            });
    }

    async uriMatches(uriRegex: RegExp) {
        await this.driver.wait(until.urlMatches(uriRegex), timeoutMillis)
            .catch(async e => {
                expect(await this.driver.getCurrentUrl()).to.match(uriRegex);
            });
    }

    el(locator: By): WebElementPromise {
        return this.driver.wait(until.elementLocated(locator), timeoutMillis);
    }

    els(locator: By): Promise<WebElement[]> {
        return this.driver.wait(until.elementsLocated(locator), timeoutMillis);
    }

    async text(el: WebElement, expectedText: string): Promise<void> {
        await this.driver.wait(until.elementTextIs(el, expectedText), timeoutMillis)
            .catch(async e => {
                const actualText = await el.getText();
                expect(actualText).to.equal(expectedText);
            });
    }
}

export function waitingOn(driver: WebDriver): WebDriverWaiter {
    return new WebDriverWaiter(driver);
}