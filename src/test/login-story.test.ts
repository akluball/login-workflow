import avaTest, { TestInterface, ExecutionContext } from 'ava';
import { By, Key, WebDriver, WebElement } from 'selenium-webdriver';
import { getDriver } from './utils';
import { waitingOn, WebDriverWaiter } from './WebDriverWaiter';
import { getEmailTo } from './email-client';
import * as uniqueData from './unique-data';

const test = avaTest as TestInterface<{ driver: WebDriver }>;

let driver: WebDriver;
let waiter: WebDriverWaiter;

function initDriverAndWaiter(t: ExecutionContext<{ driver: WebDriver }>) {
    driver = getDriver();
    waiter = waitingOn(driver);
}

test.before(initDriverAndWaiter);

test.after.always(async t => {
    await driver.quit();
});

const firstName = uniqueData.firstName();
const lastName = uniqueData.lastName();
const handle = uniqueData.handle();
const password = uniqueData.password();
const email = uniqueData.email();

test.serial('register', async t => {
    await driver.get(`${process.env.FRONTEND_URI as string}/register`);
    await waiter.el(By.className('register-name__first-input')).sendKeys(firstName);
    await waiter.el(By.className('register-name__last-input')).sendKeys(lastName);
    await waiter.el(By.className('register-name__next')).click();
    await waiter.el(By.className('register-creds__handle-input')).sendKeys(handle);
    await waiter.el(By.className('register-creds__password-input')).sendKeys(password);
    await waiter.el(By.className('register-creds__retype-password-input')).sendKeys(password);
    await waiter.el(By.className('register-creds__next')).click();
    await waiter.el(By.className('register-email__email-input')).sendKeys(email);
    await waiter.el(By.className('register-email__send')).click();
    const verificationCode = (await getEmailTo(email)).getBody().trim();
    await waiter.el(By.className('register-verify__code-input')).sendKeys(verificationCode, Key.ENTER);
    await waiter.uriMatches(/\/register\/success/);
});

test.serial('no login, /home routes to /login', async t => {
    await driver.get(`${process.env.FRONTEND_URI as string}/home`);
    await waiter.uriMatches(/\/login/);
});

test.serial('bad credentials', async t => {
    await waiter.el(By.className('login__identifier-input'))
        .sendKeys(handle);
    await waiter.el(By.className('login__password-input'))
        .sendKeys(password, 'notpassword');
    await waiter.el(By.className('login__button')).click();
    const loginErr = await waiter.el(By.className('login__below-button-err'));
    await waiter.text(loginErr, 'invalid credentials');
});

test.serial('login with email', async t => {
    const identifierInput = await waiter.el(By.className('login__identifier-input'));
    await identifierInput.clear();
    await identifierInput.sendKeys(email);
    const passwordInput = await waiter.el(By.className('login__password-input'));
    await passwordInput.clear();
    await passwordInput.sendKeys(password, Key.ENTER);
});

test.serial('email login homepage', async t => {
    await waiter.uriMatches(/\/home/);
    await waiter.title('Login Workflow - Home');
    const handleElement = await waiter.el(By.className('home__handle'));
    await waiter.text(handleElement, handle);
});

test.serial('new driver, routes to login', async t => {
    initDriverAndWaiter(t);
    await driver.get(process.env.FRONTEND_URI as string);
    await waiter.uriMatches(/\/login/);
    await waiter.title('Login Workflow - Log In');
});

test.serial('login with handle', async t => {
    const identifierInput = await waiter.el(By.className('login__identifier-input'));
    await identifierInput.clear();
    await identifierInput.sendKeys(handle);
    const passwordInput = await waiter.el(By.className('login__password-input'));
    await passwordInput.clear();
    await passwordInput.sendKeys(password, Key.ENTER);
});

test.serial('handle login homepage', async t => {
    await waiter.uriMatches(/\/home/);
    await waiter.title('Login Workflow - Home');
    const handleElement = await waiter.el(By.className('home__handle'));
    await waiter.text(handleElement, handle);
});

test.serial('logout', async t => {
    await waiter.el(By.className('home__logout')).click();
    await waiter.uriMatches(/\/login/);
    await waiter.title('Login Workflow - Log In');
});