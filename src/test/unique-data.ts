const pid = process.pid;

let firstNameCounter = 0;
export function firstName() {
    return `firstname${pid}pid${firstNameCounter++}`;
}

let lastNameCounter = 0;
export function lastName() {
    return `lastname${pid}pid${lastNameCounter++}`;
}

let handleCounter = 0;
export function handle() {
    return `handle${pid}pid${handleCounter++}`;
}

let passwordCounter = 0;
export function password() {
    return `pass${pid}pid${passwordCounter++}`;
}

let emailCounter = 0;
export function email() {
    return `user${pid}pid${emailCounter++}@example.com`;
}