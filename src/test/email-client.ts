import axios, { AxiosResponse } from 'axios';
import { sleep } from './utils';

const mailhogUri = process.env.MAILHOG_URI as string;

export class EmailNotFoundError extends Error { }

interface MailHogEmail {
    Content: {
        Body: string
    }
}

export class Email {
    constructor(private mailHogEmail: MailHogEmail) { }

    getBody() {
        return this.mailHogEmail.Content.Body;
    }
}

const retryLimit = 5;
const retryWait = 1;

export async function getEmailTo(recipient: string) {
    let tries = 0;
    while (tries < retryLimit) {
        const response = await axios.get(`${mailhogUri}/api/v2/search?kind=to&query=${recipient}`);
        if (response.data.total === 0) {
            tries++;
            await sleep(retryWait);
        } else {
            return new Email(response.data.items[0]);
        }
    }
    throw new EmailNotFoundError(`unable to find email with recipient ${recipient}`);
}