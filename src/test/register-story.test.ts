import { expect } from 'chai';
import avaTest, { TestInterface } from 'ava';
import { By, Key, WebDriver, WebElement } from 'selenium-webdriver';
import { getDriver } from './utils';
import { waitingOn, WebDriverWaiter } from './WebDriverWaiter';
import { getEmailTo, Email } from './email-client';
import * as uniqueData from './unique-data';

const test = avaTest as TestInterface<{ driver: WebDriver }>;

let driver: WebDriver;
let waiter: WebDriverWaiter;

test.before(t => {
    driver = getDriver();
    waiter = waitingOn(driver);
});

test.after.always(async t => {
    await driver.quit();
});

test.serial('click register link', async t => {
    await driver.get(process.env.FRONTEND_URI as string);
    await waiter.el(By.className('login__register-link')).click();
    await waiter.title('Login Workflow - Register Input');
});

const firstName = uniqueData.firstName();
const lastName = uniqueData.lastName();

test.serial('input name', async t => {
    await waiter.el(By.className('register-name__first-input'))
        .sendKeys(firstName);
    await waiter.el(By.className('register-name__last-input'))
        .sendKeys(lastName);
    await waiter.el(By.className('register-name__next'))
        .click();
});

const handle = uniqueData.handle();
const password = uniqueData.password();

test.serial('input credentials', async t => {
    await waiter.el(By.className('register-creds__handle-input'))
        .sendKeys(handle);
    await waiter.el(By.className('register-creds__password-input'))
        .sendKeys(password);
    await waiter.el(By.className('register-creds__retype-password-input'))
        .sendKeys(password);
    await waiter.el(By.className('register-creds__next'))
        .click();
});

const email = uniqueData.email();

test.serial('input email', async t => {
    await waiter.el(By.className('register-email__email-input'))
        .sendKeys(email);
    await waiter.el(By.className('register-email__send'))
        .click();
});

let verificationCode: string;

test.serial('fetch verification code from email', async t => {
    const verificationEmail = await getEmailTo(email);
    verificationCode = verificationEmail.getBody().trim();
});

test.serial('enter bad verification code', async t => {
    await waiter.el(By.className('register-verify__code-input'))
        .sendKeys(verificationCode, 'notthecode', Key.ENTER);
    const inputErrors = await waiter.els(By.className('register-verify__error'));
    expect(await Promise.all(inputErrors.map(async inputError => await inputError.getText())))
        .to.include('wrong verification code');
});

test.serial('enter correct verification code', async t => {
    const input = await waiter.el(By.className('register-verify__code-input'));
    await input.clear();
    await input.sendKeys(verificationCode);
    await waiter.el(By.className('register-verify__button'))
        .click();
    await waiter.uriMatches(/\/register\/success/);
});