import * as webdriver from 'selenium-webdriver';
import * as firefox from 'selenium-webdriver/firefox';
import * as chrome from 'selenium-webdriver/chrome';

export function getDriver(): webdriver.WebDriver {
    const firefoxServiceBuilder = new firefox.ServiceBuilder();
    const chromeServiceBuilder = new chrome.ServiceBuilder();
    if (process.env.WEBDRIVER_LOGGING as string === '1') {
        webdriver.logging.addConsoleHandler();
        webdriver.logging.getLogger('webdriver.http')
            .setLevel(webdriver.logging.Level.ALL);
        firefoxServiceBuilder.enableVerboseLogging().setStdio('inherit');
        chromeServiceBuilder.enableVerboseLogging().setStdio('inherit');
    }
    return new webdriver.Builder()
        .forBrowser(webdriver.Browser.CHROME) // default to chrome, override with SELENIUM_BROWSER=firefox
        .setFirefoxOptions(new firefox.Options().headless())
        .setFirefoxService(firefoxServiceBuilder)
        .setChromeOptions(new chrome.Options().headless())
        .setChromeService(chromeServiceBuilder)
        .build();
}

export function sleep(seconds: number): Promise<void> {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, seconds * 1000);
    });
}