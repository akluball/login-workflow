#!/bin/sh

dir="$(dirname "$(readlink -f "$0")")"

util() {
    "$dir/util" "$@"
}

usage='Usage:
    pipeline-down options BRANCH
BRANCH defaults to current branch
options:
    -t, --target <concourse-target>'

if [ "$1" = help ]; then
    util print-out "$usage"
    exit
fi

verify_optarg() {
    if ! util is-non-option "$2"; then
        util print-err "missing required option argument for $1"
        util print-err "$usage"
        exit 1
    fi
}

verify_set() {
    if util is-null-or-whitespace "$2"; then
        util print-err "missing required option: $1"
        util print-err "$usage"
        exit 1
    fi
}

target="$LOGIN_WORKFLOW_CONCOURSE_TARGET"
while [ "$#" -gt 0 ]; do
    case "$1" in
        --target | -t)
            verify_optarg "$1" "$2"
            shift
            target="$1"
            ;;
        --* | -*)
            util print-err "unknown option encountered: $1"
            util print-err "$usage"
            exit 1
            ;;
        *)
            if ! util is-null-or-whitespace "$branch"; then
                util print-err 'multiple BRANCH not supported'
                util print-err "$usage"
                exit 1
            fi
            branch="$1"
            ;;
    esac
    shift
done

verify_set --target "$target"
util is-null-or-whitespace "$branch" && branch="$(util current-branch)"

fly destroy-pipeline \
    --target "$target" \
    --pipeline "lw:$branch"
