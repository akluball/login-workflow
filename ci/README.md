# Login Workflow - Continuous Integration

Continuous integration is performed using [concourse].
A buildkit instance and docker registry are also required.

On a linux host with docker, this infrastructure can be setup for local ci using [ci-side-by-side].

## Infrastructure

To setup with [ci-side-by-side]:
```
sbs network up
sbs concourse up --login <concourse-target>
sbs registry up
sbs buildkit up
```

To teardown:
```
sbs buildkit down
sbs registry down
sbs concourse down
sbs network down
```

## Pipelines

Some options have environment defaults:
- `LOGIN_WORKFLOW_CONCOURSE_TARGET` for `<concourse-target>`
- `LOGIN_WORKFLOW_SSH_KEY` for `<ssh-key-file>`
- `SBS_BUILDKIT_ADDR` for `<buildkit-addr>`
- `SBS_REGISTRY` for `<docker-registry>`

If [ci-side-by-side] is used, then the `SBS*` environment variables can be
loaded into the environment with:
```
$(sbs env exports)
```

Create pipeline (branch defaults to current branch):
```
./pipeline-up \
    --target <concourse-target> \
    --ssh-key <ssh-key-file> \
    --buildkit-addr <buildkit-addr> \
    --registry <docker-registry> \
    BRANCH
```
By default, the pipeline triggers firefox and chrome tests on repository changes.
To only trigger chrome, pass the `--chrome` option.
Likewise, pass the `--firefox` option to only trigger firefox.
To turn on webdriver logging, pass `--webdriver-logging`.

Destroy pipeline (branch defaults to current branch):
```
./pipeline-down \
    --target <concourse-target> \
    BRANCH
```

## Todo

Currently, the frontend and backend images are built on each commit to the login-workflow branch.
This is so that the tests will run against input images from the same commit.
However, I really only need to rebuild the frontend and backend images on changes to corresponding 
login-workflow/commit/* files.

[concourse]: https://concourse-ci.org/

[ci-side-by-side]: https://gitlab.com/akluball/ci-side-by-side
