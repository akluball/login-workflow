# Login Workflow

This application serves as an example of a containerized development workflow.
There are two component projects which are [login-workflow-frontend] and [login-workflow-backend].
There are also two projects used to perform ci which are [ci-side-by-side] and [oci-tools].
This repository contains scripts for deployment and running tests against that deployment.

## Requirements

This project requires a linux host with docker and node/npm.

## Containerized Workflow

The containerized workflow manifests itself in the integration tests of the component projects and this root project.
There are test runners (shell scripts) that build and run the application-under-test container(s).
The test runner then builds and runs the test container (where the tests are run).
Additionally, any other infrastructure required for tests is deployed in containers prior to running the tests.

The takeaway should be that everything is running in containers besides the test runner shell script.
This allows for some nice things like minimizing host machine dependencies, networking (dns) without messing with host
machine network configuration, and easy cleanup in general.
Not only is it a clean workflow but it brings the test/dev environment closer to what a production environment would
look like.

## Deployment

The `live-chrome` and `test-runner` commands (described below) both perform deployment and undeployment of the application.
Deployment consists of the following steps:
1. Pulls [login-workflow-frontend] and [login-workflow-backend] into [repos] directory
2. Checks out configured [commits]
3. Builds frontend and backend images
4. Create docker network to run containers on
5. Runs detached Postgres container
6. Runs detached MailHog container
7. Runs detached frontend container
8. Runs detached backend container

Undeployment consists of stopping all containers and deleting the network.

## Live Chrome

Deploy the application and open the frontend with `google-chrome`:
```
npm run live-chrome
```
This assumes the `google-chrome` binary is in path and will require upgraded privileges (will be prompted for this).
This script does the following:
1. Deploy application as described above
2. Unshares the mount namespace
3. Mounts the frontend containers networking namespace so that `ip` command can "see it"
4. Mounts frontend containers resolver configuration to host
5. Opens frontend application using google-chrome in frontend containers networking namespace
6. Once google chrome closed, undeploys application as described above

Essentially, this gives us a browser session with docker network dns.

To verify the application is functioning, you can register, log in, then log out.
During registration, you will be prompted to enter an email address.
A verification code email is sent from the application and captured by [MailHog] with that entered email address as the recipient.
The MailHog ui will be open in a second tab.
Simply copy the verification code from the captured email and enter it in the prompt.

## Test

To run tests
```
npm run test-runner
```
This command runs a script that does the following:
1. Deploys application as described above
2. Builds test image
3. Runs detached selenium container
4. Runs tests container
5. Deletes selenium container
6. Undeploys application as described above
7. Exits with exit code of test container

The selenium browser can be configured using `SELENIUM_BROWSER` environment variable.
The supported values are `chrome` and `firefox` and defaults to `chrome`
To turn on webdriver logging, set `WEBDRIVER_LOGGING=1`.

## Development Mode Commands

There are development mode commands for both `live-chrome` and `test-runner`.
First, the repos must be pulled to a different location than the non-development mode repos.

Before executing any other development mode command, pull [login-workflow-frontend] and [login-workflow-backend] into [dev-repos]:
```
npm run pull-repos:dev
```
This command does NOT checkout the commit configured in [commits].
To develop a non-master branch, it will have to be checked out manually.
This command will fail if [dev-repos] exists so that existing work is not overwritten.

Then, to run live-chrome in development mode:
```
npm run live-chrome:dev
```
The frontend and backend are deployed in a way that allows them to see changes to the source code in [dev-repos].

The backend source code is mounted into the backend container.
The development mode backend image build uses a different multi-stage target of the same Dockerfile as the non-development mode build.
This multi-stage target has `quarkusDev` as an entry point which picks up changes to the source code.
Ultimately, changes to backend source code in [dev-repos] are picked up by the backend container.

For the frontend deployment, a frontend compiler (running in watch mode) is run in a separate container.
The frontend source code is mounted into the compiler container.
A named volume is mounted into the compiler container, encapsulating the compilation destination.
This named volume is also mounted into the frontend container.
Therefore, the frontend container sees changes to source code in [dev-repos] via the compiler container.

To run the test-runner in development mode
```
npm run test-runner:dev
```

The frontend and backend application are deployed as described above so they are seeing changes to [dev-repos].
Like the frontend deployment, a test compiler (running in watch mode) is run in a separate container.
The test source code is mounted into the compiler container.
A named volume is mounted into the compiler container, encapsulating the compilation destination.
This named volume is also mounted into the test container.
Finally, the entrypoint of the test container is overridden with an interactive shell.
Then tests can be repeatedly run in container:
```
npm test
```
As described above, these tests runs will see changes to the frontend and backend source code in [dev-repos]
and the test code.
When finished with development, simply exit the interactive shell and everything will be undeployed.

## Development Mode Logging

To view logs of the backend container:
```
npm run backend-logs
```

To view logs of the frontend compiler container:
```
npm run compiler-logs:dev
```

Likewise for the logs of the test compiler container:
```
npm run test-compiler-logs:dev
```

These npm scripts simply wrap `docker logs` calls.
Therefore, you can pass any `docker logs` options to the npm run calls.
So if you want to follow the compile logs:
```
npm run compiler-logs:dev -- --follow
```

## Test Runner Logging

Most logging is suppressed in the test runner such as the output from docker build.
To turn this logging on, use environment `LOGIN_WORKFLOW_LOGGING=1`.
So to run tests with docker build logging:
```
LOGIN_WORKFLOW_LOGGING=1 npm run test-runner
```

## Bump Commits

When ready to bump [commits] to the currently checked out commits in [dev-repos]:
```
npm run dev-bump
```

## Docker Image Cleanup

Remove built images:
```
npm run rm-built-images
```

Remove dependency images:
```
npm run rm-dependency-images
```

## Notes

Mock Server requests occasionally terminate with socket hang up.

[login-workflow-frontend]: https://gitlab.com/akluball/login-workflow-frontend
[login-workflow-backend]: https://gitlab.com/akluball/login-workflow-backend
[oci-tools]: https://gitlab.com/akluball/oci-tools
[ci-side-by-side]: https://gitlab.com/akluball/ci-side-by-side

[commits]: commits
[repos]: repos
[dev-repos]: dev-repos

[MailHog]: https://github.com/mailhog/MailHog
